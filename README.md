Demonstrates how to use Scrapy together with S3-compatible API service like Minio
to push the scraped data to the store. It pushes
each item to a unique object, and each bucket corresponds to the spider name.
